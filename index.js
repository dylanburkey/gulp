const express = require('express')
path = require('path')
serveStatic = require('serve-static')
app = express()

app.use(serveStatic(path.join(__dirname, '/build')))
app.listen(process.env.PORT || 3000)