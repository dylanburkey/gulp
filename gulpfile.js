// var gulp = require('gulp');
// plumber = require('gulp-plumber'),
//     rename = require('gulp-rename');
// var sass = require('gulp-sass');
// var minifycss = require('gulp-minify-css');
// var htmlmin = require('gulp-htmlmin');
// var autoprefixer = require('gulp-autoprefixer');
// var babel = require('gulp-babel');
// var concat = require('gulp-concat');
// var jshint = require('gulp-jshint');
// var uglify = require('gulp-uglify');
// var nunjucksRender = require('gulp-nunjucks-render');
// var newer = require('gulp-newer');
// var data = require('gulp-data');
// const imagemin = require('gulp-imagemin');

const gulp = require('gulp'),
    newer = require('gulp-newer'),
    imagemin = require('gulp-imagemin'),
    htmlclean = require('gulp-htmlclean'),
    stripdebug = require('gulp-strip-debug'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    assets = require('postcss-assets'),
    autoprefixer = require('autoprefixer'),
    mqpacker = require('css-mqpacker'),
    cssnano = require('cssnano');



// var browserSync = require('browser-sync').create();

devBuild = (process.env.NODE_ENV !== 'production'),

    // folders
    folder = {
        src: 'src/',
        build: 'build/'
    };


gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: './dist'
        },
    })
})

gulp.task('bs-reload', function() {
    browserSync.reload();
});


// image processing
gulp.task('images', function() {
    var out = folder.build + 'images/';
    return gulp.src(folder.src + 'images/**/*')
        .pipe(newer(out))
        .pipe(imagemin({ optimizationLevel: 5 }))
        .pipe(gulp.dest(out));
});

gulp.task('html', ['images'], function() {
    var
        out = folder.build + 'html/',
        page = gulp.src(folder.src + 'html/**/*')
        .pipe(newer(out));

    // minify production code
    if (!devBuild) {
        page = page.pipe(htmlclean());
    }

    return page.pipe(gulp.dest(out));
});

//JavaScript processing
gulp.task('js', function() {

    var jsbuild = gulp.src(folder.src + 'js/**/*')
        .pipe(deporder())
        .pipe(concat('main.js'));

    if (!devBuild) {
        jsbuild = jsbuild
            .pipe(stripdebug())
            .pipe(uglify());
    }

    return jsbuild.pipe(gulp.dest(folder.build + 'js/'));

});

// CSS processing
gulp.task('css', ['images'], function() {

    var postCssOpts = [
        assets({ loadPaths: ['images/'] }),
        autoprefixer({ browsers: ['last 2 versions', '> 2%'] }),
        mqpacker
    ];

    if (!devBuild) {
        postCssOpts.push(cssnano);
    }

    return gulp.src(folder.src + 'scss/main.scss')
        .pipe(sass({
            outputStyle: 'nested',
            imagePath: 'images/',
            precision: 3,
            errLogToConsole: true
        }))
        .pipe(postcss(postCssOpts))
        .pipe(gulp.dest(folder.build + 'css/'));

});







// gulp.task('minify', function() {
//     return gulp.src('./*.html')
//         .pipe(htmlmin({
//             collapseWhitespace: true
//         }))
//         .pipe(gulp.dest('dist/'));
// });

// gulp.task('nunjucks', function() {
//     return gulp.src('src/assets/templates/pages/**/*.+(html|njk|nunjucks)')
//         .pipe(data(function() {
//             return require('./src/assets/templates/data/data.json');
//         }))
//         .pipe(nunjucksRender({
//             path: ['./src/assets/templates/']
//         }))
//         .pipe(gulp.dest('./dist/'))
//         .pipe(browserSync.reload({
//             stream: true
//         }))


// });



// gulp.task('images', function() {
//     return gulp.src('src/assets/media/images/**/*.+(png|jpg|gif|svg)')
//         .pipe(imagemin())
//         .pipe(gulp.dest('dist/assets/media/images/'))
// });


// gulp.task('sass', function() {
//     gulp.src(['src/assets/styles/**/*.scss'])
//         .pipe(sass({
//             outputStyle: 'compressed'
//         }).on('error', sass.logError))
//         .pipe(autoprefixer('last 3 versions'))
//         .pipe(gulp.dest('dist/assets/styles/'))
//         .pipe(rename({
//             suffix: '.min'
//         }))
//         .pipe(gulp.dest('dist/assets/styles/'))
//         .pipe(browserSync.reload({
//             stream: true
//         }))
// });
// gulp.task('scripts', function() {
//     return gulp.src('src/assets/scripts/**/*.js')
//         .pipe(plumber({
//             errorHandler: function(error) {
//                 console.log(error.message);
//                 this.emit('end');
//             }
//         }))
//         .pipe(jshint())
//         .pipe(jshint.reporter('default'))
//         .pipe(concat('main.js'))
//         .pipe(babel())
//         .pipe(gulp.dest('dist/assets/scripts/'))
//         .pipe(rename({
//             suffix: '.min'
//         }))
//         .pipe(uglify())
//         .pipe(gulp.dest('dist/assets/scripts/'))
//         .pipe(browserSync.reload({
//             stream: true
//         }))
// });


gulp.task('watch', ['browserSync', 'css', 'js', 'html', 'images'], function() {
    gulp.watch(folder.src + 'images/**/*', ['images']);

    // html changes
    gulp.watch(folder.src + 'html/**/*', ['html']);

    // javascript changes
    gulp.watch(folder.src + 'js/**/*', ['js']);

    // css changes
    gulp.watch(folder.src + 'scss/**/*', ['css']);

    // Other watchers
});